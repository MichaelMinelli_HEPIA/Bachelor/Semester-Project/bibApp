import Foundation
import CoreLocation

/**
 *	@author Michaël Minelli, 04.02.2016
 *
 *  Protocol which discribe a class which respond to event like a new beacon is detect from being the most near or the smartphone enter in a region
 */
public protocol BeaconDetectionDelegate {
    func beaconDetection(mainBeaconChange beacon:Beacon)
    func beaconDetection(enterRegion region:CLRegion)
}