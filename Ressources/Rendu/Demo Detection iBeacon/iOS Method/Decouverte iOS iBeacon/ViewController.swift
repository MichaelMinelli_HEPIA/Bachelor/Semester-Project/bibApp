//
//  ViewController.swift
//  Decouverte iOS iBeacon
//
//  Created by Michaël Minelli on 22.10.15.
//  Copyright © 2015 Michaël Minelli. All rights reserved.
//

import UIKit
import CoreLocation

class ViewController: UIViewController, CLLocationManagerDelegate {

    @IBOutlet weak var txtShow: UITextView!
    
    var locationManager: CLLocationManager!
    
    //CLBeaconRegion(proximityUUID: NSUUID(UUIDString: "B9407F30-F5F8-466E-AFF9-25556B57FE6D")!, major: CLBeaconMajorValue(402), minor: CLBeaconMinorValue(1), identifier: "ch.minelli.Decouverte-iOS-iBeacon.ViewController") //Beacon bleu clair
    var beaconRegion: CLBeaconRegion! = CLBeaconRegion(proximityUUID: NSUUID(UUIDString: "B9407F30-F5F8-466E-AFF9-25556B57FE6D")!, identifier: "ch.minelli.Decouverte-iOS-iBeacon.ViewController")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Création du Location Manager ainsi qu'indication à celui-ci que son delegate est l'objet courant
        locationManager = CLLocationManager()
        locationManager.delegate = self
        
        //Permet d'indiquer au système de toujours chercher les beacons même lorsque l'application est fermée
        beaconRegion.notifyOnEntry = true
        beaconRegion.notifyOnExit = true
        
        //Vérifie et demande les droits nécessaires à la localisation
        if CLLocationManager.locationServicesEnabled() {
            if CLLocationManager.authorizationStatus() ==  CLAuthorizationStatus.NotDetermined {
                locationManager.requestAlwaysAuthorization() //Demande l'autorisation de toujours pouvoir chercher des balises
            } else if CLLocationManager.authorizationStatus() == CLAuthorizationStatus.Denied {
                let message = UIAlertView(title: "Accès refusé", message: "Vous avez refusé l'accès à la localisation", delegate: nil, cancelButtonTitle: "Ok")
                message.show()
            } else if CLLocationManager.authorizationStatus() == CLAuthorizationStatus.Restricted {
                let message = UIAlertView(title: "Localisation non disponible", message: "Nous n'avons pas accès à la localisation", delegate: nil, cancelButtonTitle: "Ok")
                message.show()
            }
            
            //Lance la « localisation »
            locationManager.startMonitoringForRegion(self.beaconRegion)
        } else {
            let message = UIAlertView(title: "Erreur de localisation", message: "Localisation non activée", delegate: nil, cancelButtonTitle: "Ok")
            message.show()
        }
    }
    
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        if status != CLAuthorizationStatus.NotDetermined && status != CLAuthorizationStatus.Denied {
            locationManager.startMonitoringForRegion(self.beaconRegion)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidDisappear(animated: Bool) {
        locationManager.stopRangingBeaconsInRegion(self.beaconRegion)
    }
    
    // Est appelée quand un beacon commence à être détecté (entre dans la région de réception)
    func locationManager(manager: CLLocationManager, didEnterRegion region: CLRegion) {
        let notification = UILocalNotification()
        notification.alertBody = "Entree dans la région" // text that will be displayed in the notification
        notification.alertAction = "région" // text that is displayed after "slide to..." on the lock screen - defaults to "slide to view"
        notification.soundName = UILocalNotificationDefaultSoundName
        notification.category = "REGION_CATEGORY"
        UIApplication.sharedApplication().presentLocalNotificationNow(notification)
    }
    
    // Est appelée quand plus aucun beacon n'est détecté (sort de la région de réception)
    func locationManager(manager: CLLocationManager, didExitRegion region: CLRegion) {
        let notification = UILocalNotification()
        notification.alertBody = "Sortie de la région" // text that will be displayed in the notification
        notification.alertAction = "région" // text that is displayed after "slide to..." on the lock screen - defaults to "slide to view"
        notification.soundName = UILocalNotificationDefaultSoundName // play default sound
        //notification.userInfo = ["UUID": item.UUID, ] // assign a unique identifier to the notification so that we can retrieve it later
        notification.category = "REGION_CATEGORY"
        UIApplication.sharedApplication().presentLocalNotificationNow(notification)
    }
    
    func locationManager(manager: CLLocationManager, didStartMonitoringForRegion region: CLRegion) {
        //Demande également les mises à jour sur l'état de la région
        locationManager.requestStateForRegion(region)
    }
    
    func locationManager(manager: CLLocationManager, didDetermineState state: CLRegionState, forRegion region: CLRegion) {
        //Si nous sommes à proximité de balises, commence la détection des balises en continu sinon la désactive
        
        if state == CLRegionState.Inside {
            locationManager.startRangingBeaconsInRegion(beaconRegion)
        }
        else {
            locationManager.stopRangingBeaconsInRegion(beaconRegion)
        }
    }
    
    // Voilà la partie la plus intéréssante, la découverte des balises
    func locationManager(manager: CLLocationManager, didRangeBeacons beacons: [CLBeacon], inRegion region: CLBeaconRegion) {
        //Extrait tous les beacons dont on connaît la distance
        let knownBeacons = beacons.filter{ $0.proximity != CLProximity.Unknown }
        for beacon in knownBeacons {
            var proximityMessage: String
            
            switch beacon.proximity {
            case CLProximity.Immediate:
                proximityMessage = "Très proche"
                
            case CLProximity.Near:
                proximityMessage = "Proche"
                
            case CLProximity.Far:
                proximityMessage = "Loin"
                
            default:
                proximityMessage = "Pas possible avec le filtre plus haut"
            }
            
            txtShow.text = "\(txtShow.text)\n\(beacon.major).\(beacon.minor) \(proximityMessage) +/- \(beacon.accuracy)m \(beacon.rssi) "
        }
    }
    
    //Gestion des erreurs
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        let message = UIAlertView(title: "Erreur de monitoring", message: error.localizedDescription, delegate: nil, cancelButtonTitle: "Ok")
        message.show()
    }

    func locationManager(manager: CLLocationManager, monitoringDidFailForRegion region: CLRegion?, withError error: NSError) {
        let message = UIAlertView(title: "Erreur de monitoring", message: error.localizedDescription, delegate: nil, cancelButtonTitle: "Ok")
        message.show()
    }

    func locationManager(manager: CLLocationManager,rangingBeaconsDidFailForRegion region: CLBeaconRegion,withError error: NSError){
        let message = UIAlertView(title: "Erreur de ranging", message: error.localizedDescription, delegate: nil, cancelButtonTitle: "Ok")
        message.show()
    }
}

