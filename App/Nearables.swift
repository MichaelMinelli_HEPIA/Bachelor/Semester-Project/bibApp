import Foundation

/**
 *	@author Michaël Minelli, 16.01.2016
 *
 *	Datas of a nearables beacon
 */
open class Nearables: NSObject, Beacon, NSCoding {
    open var section: Int?
    
    open var id: String
    
    init(section: Int?, id: String) {
        self.section = section
        self.id = id
    }
    
    public required init(coder aDecoder: NSCoder) {
        self.section = aDecoder.decodeInteger(forKey: "section")
        self.id = aDecoder.decodeObject(forKey: "id") as! String
    }
    
    open func encode(with aCoder: NSCoder) {
        aCoder.encode(self.section!, forKey: "section")
        aCoder.encode(self.id, forKey: "id")
    }
}
