import Foundation

/**
 *	@author Michaël Minelli, 08.02.2016
 *
 *	Class that describe the beacon detection preference
 *  Implement NSCoding for the possibility to put an object of this class in preferences (it's like a serialization)
 */
open class BeaconDetectionPreference: NSObject, NSCoding {
    //Indicate the moment when the user disabled the detection
    open var timestamp: Int = 0
    //Indicate the time (in secondes) he want to be disabled
    open var duration: Int = 0
    
    required public override init() {
        super.init()
    }
    
    required public init(coder aDecoder: NSCoder) {
        //Get information from the coder
        self.timestamp = aDecoder.decodeInteger(forKey: "timestamp")
        self.duration = aDecoder.decodeInteger(forKey: "duration")
    }
    
    open func encode(with aCoder: NSCoder) {
        //Put informations in the coder
        aCoder.encode(self.timestamp, forKey: "timestamp")
        aCoder.encode(self.duration, forKey: "duration")
    }
    
    /**
     Function that return the time (in seconde) since the desactivation of the beacon detection
     
     - returns: The time in second (Double) from the desactivation
     */
    open func timeIntervaelSinceDesactivation() -> Double {
        return Date().timeIntervalSince1970 - Double(self.timestamp)
    }
    
    /**
     Enable the beacon detection
     */
    open func enable() {
        self.timestamp = 0
        self.duration = 0
    }
    
    /**
     Disable the beacon detection
     
     - parameter duration: Time in second of desactivation
     */
    open func disable(_ duration: Int) {
        self.timestamp = Int(Date().timeIntervalSince1970)
        self.duration = duration
    }
    
    /**
     Test if the beacon detection is wanted by the user
     
     - parameter pref: Optional Preferences object for do the upodate of the state of the time of desactivation is left
     
     - returns: Boolean that indicate if the beacon detection is enable
     */
    open func isEnabled(_ pref: Preferences? = nil) -> Bool {
        //If the duration is equal to 0 the detection is enables otherwise we have to verify the time
        if duration != 0 {
            //Test if the time since desactivation is less than the time wanted by the user
            if Int(self.timeIntervaelSinceDesactivation()) < duration {
                //If yes, the beacon detection is still disabled
                return false
            } else {
                //If no, enable the beacon detection
                self.enable()
                
                //Update if possible the preferences
                if let pref = pref {
                    pref.setBeaconDetectionPref(self)
                }
            }
        }
        
        return true
    }
}
