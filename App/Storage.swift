import Foundation

/**
 *	@author Michaël Minelli, 13.02.2016
 *
 *	Interaction with the local storage (actually used for storage of sections, books and beacons)
 */
open class Storage {
    fileprivate static let DATA_STORAGE_DATE_STOR_ID = "DataStorageDateStor"
    fileprivate static let DATA_STORAGE_DATE_STOR_DEFAULT = 0
    fileprivate static let TIME_OF_VALIDITY = 1 * 24 * 60 * 60
    
    fileprivate static let DATA_LIST_SECTIONS_STOR_ID = "ListSectionsStor"
    fileprivate static let DATA_LIST_BOOKS_STOR_ID = "ListBooksStor"
    fileprivate static let DATA_LIST_IBEACONS_STOR_ID = "ListiBeaconsStor"
    fileprivate static let DATA_LIST_NEARABLES_STOR_ID = "ListNearablesStor"
    fileprivate static let DATA_LIST_EDDYSTONES_STOR_ID = "ListEddystonesStor"
    
    fileprivate let standardDefault = UserDefaults.standard
    
    init() {
        self.standardDefault.register(defaults: [Storage.DATA_STORAGE_DATE_STOR_ID: Storage.DATA_STORAGE_DATE_STOR_DEFAULT])
    }
    
    /**
     Indicate if data need to be updated (if the last update is expired, time difference bigger than TIME_OF_VALIDITY)
     
     - returns: true if data need to be updated, false if datas are up to date
     */
    func isNeedToUpdateData() -> Bool {
        return Int(Date().timeIntervalSince1970) - self.standardDefault.integer(forKey: Storage.DATA_STORAGE_DATE_STOR_ID) > Storage.TIME_OF_VALIDITY
    }
    
    /**
     Update the time of data storage
     */
    func dataUpdated() {
        self.standardDefault.set(Int(Date().timeIntervalSince1970), forKey: Storage.DATA_STORAGE_DATE_STOR_ID)
    }
    
    /**
     Get all sections store on local storage
     
     - returns: List of section if found
     */
    func getSections() -> [Section]? {
        NSKeyedUnarchiver.setClass(Section.self, forClassName: "BibAppSection")
        if let dataToLoad = self.standardDefault.object(forKey: Storage.DATA_LIST_SECTIONS_STOR_ID) as? Data {
            return NSKeyedUnarchiver.unarchiveObject(with: dataToLoad) as? [Section]
        } else {
            return nil
        }
    }
    
    /**
     Store all sections on local storage
     
     - parameter sections: sections list to store
     */
    func storeSections(_ sections: [Section]) {
        NSKeyedArchiver.setClassName("BibAppSection", for: Section.self)
        let dataToStore: Data = NSKeyedArchiver.archivedData(withRootObject: sections)
        self.standardDefault.set(dataToStore, forKey: Storage.DATA_LIST_SECTIONS_STOR_ID)
        self.standardDefault.synchronize()
    }
    
    /**
     Get all books store on local storage
     
     - returns: List of book if found
     */
    func getBooks() -> [Book]? {
        NSKeyedUnarchiver.setClass(Book.self, forClassName: "BibAppBook")
        if let dataToLoad = self.standardDefault.object(forKey: Storage.DATA_LIST_BOOKS_STOR_ID) as? Data {
            return NSKeyedUnarchiver.unarchiveObject(with: dataToLoad) as? [Book]
        } else {
            return nil
        }
    }
    
    /**
     Store all books on local storage
     
     - parameter books: book list to store
     */
    func storeBooks(_ books: [Book]) {
        NSKeyedArchiver.setClassName("BibAppBook", for: Book.self)
        let dataToStore: Data = NSKeyedArchiver.archivedData(withRootObject: books)
        self.standardDefault.set(dataToStore, forKey: Storage.DATA_LIST_BOOKS_STOR_ID)
        self.standardDefault.synchronize()
    }
    
    /**
     Get all iBeacons store on local storage
     
     - returns: List of iBeacons if found
     */
    func getiBeacons() -> [iBeacon]? {
        NSKeyedUnarchiver.setClass(iBeacon.self, forClassName: "BibAppiBeacon")
        if let dataToLoad = self.standardDefault.object(forKey: Storage.DATA_LIST_IBEACONS_STOR_ID) as? Data {
            return NSKeyedUnarchiver.unarchiveObject(with: dataToLoad) as? [iBeacon]
        } else {
            return nil
        }
    }
    
    /**
     Store all iBeacons on local storage
     
     - parameter ibeacons: book list to store
     */
    func storeiBeacons(_ ibeacons: [iBeacon]) {
        NSKeyedArchiver.setClassName("BibAppiBeacon", for: iBeacon.self)
        let dataToStore: Data = NSKeyedArchiver.archivedData(withRootObject: ibeacons)
        self.standardDefault.set(dataToStore, forKey: Storage.DATA_LIST_IBEACONS_STOR_ID)
        self.standardDefault.synchronize()
    }
    
    /**
     Get all nearables store on local storage
     
     - returns: List of nearables if found
     */
    func getNearables() -> [Nearables]? {
        NSKeyedUnarchiver.setClass(Nearables.self, forClassName: "BibAppNearables")
        if let dataToLoad = self.standardDefault.object(forKey: Storage.DATA_LIST_NEARABLES_STOR_ID) as? Data {
            return NSKeyedUnarchiver.unarchiveObject(with: dataToLoad) as? [Nearables]
        } else {
            return nil
        }
    }
    
    /**
     Store all nearables on local storage
     
     - parameter nearables: nearables list to store
     */
    func storeNearables(_ nearables: [Nearables]) {
        NSKeyedArchiver.setClassName("BibAppNearables", for: Nearables.self)
        let dataToStore: Data = NSKeyedArchiver.archivedData(withRootObject: nearables)
        self.standardDefault.set(dataToStore, forKey: Storage.DATA_LIST_NEARABLES_STOR_ID)
        self.standardDefault.synchronize()
    }
    
    /**
     Get all eddystones store on local storage
     
     - returns: List of eddystones if found
     */
    func getEddystones() -> [Eddystone]? {
        NSKeyedUnarchiver.setClass(Eddystone.self, forClassName: "BibAppEddystone")
        if let dataToLoad = self.standardDefault.object(forKey: Storage.DATA_LIST_EDDYSTONES_STOR_ID) as? Data {
            return NSKeyedUnarchiver.unarchiveObject(with: dataToLoad) as? [Eddystone]
        } else {
            return nil
        }
    }
    
    /**
     Store all eddystones on local storage
     
     - parameter eddystones: eddystone list to store
     */
    func storeEddystones(_ eddystones: [Eddystone]) {
        NSKeyedArchiver.setClassName("BibAppEddystone", for: Eddystone.self)
        let dataToStore: Data = NSKeyedArchiver.archivedData(withRootObject: eddystones)
        self.standardDefault.set(dataToStore, forKey: Storage.DATA_LIST_EDDYSTONES_STOR_ID)
        self.standardDefault.synchronize()
    }
}
