//
//  ViewController.swift
//  Demon iBeacon
//
//  Created by Michaël Minelli on 20.11.15.
//  Copyright © 2015 Michaël Minelli. All rights reserved.
//

import UIKit

class ViewController: UIViewController, iBeaconDetectionDelegate {
    
    @IBOutlet weak var txtShow: UITextView!
    @IBOutlet weak var img: UIImageView!
    
    var ibeaconDetection: iBeaconDetection?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ibeaconDetection = iBeaconDetection(viewController: self, actualMinor: 0, delegate: self)
        ibeaconDetection!.run()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidDisappear(animated: Bool) {
        //locationManager.stopRangingBeaconsInRegion(self.beaconRegion)
    }
    
    func onMinorChange(newMinor: Int) {
        //txtShow.text = txtShow.text + "\n" + String(newMinor)
        
        if newMinor == 1 {
            self.view.backgroundColor = UIColor.greenColor()
        } else if newMinor == 2 {
            self.view.backgroundColor = UIColor.redColor()
        } else {
            self.view.backgroundColor = UIColor.whiteColor()
        }
        
        self.img.image = UIImage(named: String(newMinor))
    }
    
}