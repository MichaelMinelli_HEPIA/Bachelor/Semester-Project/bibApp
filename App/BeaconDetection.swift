import Foundation

/**
 *	@author Michaël Minelli, 04.02.2016
 *
 *  Protocol which discribe a class which use a delegate (BeaconDetectionDelegate) for event like new beacon detection
 *  This is a protocol for the case where we want to support more than only iBeacon but other protocol too.
 */
public protocol BeaconDetection {
    var delegate: BeaconDetectionDelegate { get set }
}