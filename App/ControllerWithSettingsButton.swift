import Foundation

/**
 *	@author Michaël Minelli, 04.02.2016
 *
 *  Describe all view controller with a settings button in navigation bar
 */
public protocol ControllerWithSettingsButton {
    func settingsButtonClick(_ sender: AnyObject)
}
