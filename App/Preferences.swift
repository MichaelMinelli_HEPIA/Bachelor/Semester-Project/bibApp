import Foundation

/**
 *	@author Michaël Minelli, 06.02.2016
 *
 *	Section changement class (respond to the "BeaconDetectionDelegate" delegate)
 */
open class Preferences {
    
    fileprivate static let BOOK_LIST_STYLE_MIN_ID = "BookListStyleMin"
    fileprivate static let BEACON_DETECTION_PREF_ID = "BeaconDetectionPref"
    
    fileprivate static let BOOK_LIST_STYLE_MIN_DEFAULT = false
    fileprivate static let BEACON_DETECTION_PREF_DEFAULT = BeaconDetectionPreference()
    
    //Defaults value of preference
    fileprivate static let DEFAULTS:[String : AnyObject] = [Preferences.BOOK_LIST_STYLE_MIN_ID: Preferences.BOOK_LIST_STYLE_MIN_DEFAULT as AnyObject]
    
    fileprivate let standardDefault = UserDefaults.standard
    
    /**
     Register default values of preferences
     */
    func registerDefaults() {
        //Because the registerDefaults function of NSUserDefaults do not support default preference for object do ourself
        //Verify that the preference key have not a value and affect the defaults value in preference
        if self.standardDefault.object(forKey: Preferences.BEACON_DETECTION_PREF_ID) == nil {
            self.setBeaconDetectionPref(Preferences.BEACON_DETECTION_PREF_DEFAULT)
        }
        
        self.standardDefault.register(defaults: Preferences.DEFAULTS)
    }
    
    /**
     Indicate if the user want the min style for book list
     
     - returns: Boolean, true for the min style and false for the style with image
     */
    func isBookListMinStyle() -> Bool {
        return self.standardDefault.bool(forKey: Preferences.BOOK_LIST_STYLE_MIN_ID)
    }
    
    /**
     Set the preference for the min style book list
     
     - parameter minStyle: Boolean, true for the min style and false for the style with image
     */
    func setBookListMinStyle(_ minStyle: Bool) {
        self.standardDefault.set(minStyle, forKey: Preferences.BOOK_LIST_STYLE_MIN_ID)
        self.standardDefault.synchronize()
    }
    
    /**
     Recuperate the user preference for the beacon detection
     
     - returns: An object of the class BeaconDetectionPreference
     */
    func getBeaconDetectionPref() -> BeaconDetectionPreference {
        NSKeyedUnarchiver.setClass(BeaconDetectionPreference.self, forClassName: "BeaconDetectionPreference")
        let dataToLoad = self.standardDefault.object(forKey: Preferences.BEACON_DETECTION_PREF_ID) as! Data
        return NSKeyedUnarchiver.unarchiveObject(with: dataToLoad) as! BeaconDetectionPreference
    }
    
    /**
     Set the user preference for the beacon detection
     
     - parameter beaconDetectionPreference: A object of the class BeaconDetectionPreference to store in preferences
     */
    func setBeaconDetectionPref(_ beaconDetectionPreference: BeaconDetectionPreference) {
        NSKeyedArchiver.setClassName("BeaconDetectionPreference", for: BeaconDetectionPreference.self)
        let dataToStore: Data = NSKeyedArchiver.archivedData(withRootObject: beaconDetectionPreference)
        self.standardDefault.set(dataToStore, forKey: Preferences.BEACON_DETECTION_PREF_ID)
        self.standardDefault.synchronize()
    }
}
