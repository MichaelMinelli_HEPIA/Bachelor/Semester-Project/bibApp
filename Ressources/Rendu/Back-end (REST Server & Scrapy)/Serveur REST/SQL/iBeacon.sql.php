<?php

class iBeaconSQL extends SQLConnect
{
    private static $getAll, $getBooksForBeacon, $getBooks;

    public static function prepare() {
        self::$getAll = self::$db->prepare(
            "SELECT ibeacon_major as 'major', ibeacon_minor as 'minor', ibeacon_section as 'section' FROM iBeacon WHERE ibeacon_status = 1 ORDER BY ibeacon_major, ibeacon_minor;"
        );
        
        self::$getBooksForBeacon = self::$db->prepare(
	        "SELECT book_id as 'id', book_title as 'title', book_authors as 'author', book_quote as 'quote', book_image as 'image', book_resume as 'resume', book_language as 'language', book_applicant as 'applicant', book_year as 'year', book_description as 'description', book_editor as 'editor', book_section as 'section' FROM Book JOIN iBeacon ON book_section = iBeacon_section WHERE ibeacon_major=:major AND ibeacon_minor=:minor ORDER BY book_insert_date DESC;"
        );
        
        self::$getBooks = self::$db->prepare(
            "SELECT book_id as 'id', book_title as 'title', book_authors as 'author', book_quote as 'quote', book_image as 'image', book_resume as 'resume', book_language as 'language', book_applicant as 'applicant', book_year as 'year', book_description as 'description', book_editor as 'editor', book_section as 'section' FROM Book WHERE book_section IN (SELECT DISTINCT(ibeacon_section) FROM iBeacon WHERE ibeacon_status = 1) ORDER BY book_insert_date DESC;"
        );
    }

    public static function getAll() {
        self::$getAll->execute();

        return self::$getAll->fetchAll();
    }
    
    public static function getBooksForBeacon($major, $minor) {
        self::$getBooksForBeacon->execute(array(
	        ':major' => $major,
	        ':minor' => $minor
        ));

        return self::$getBooksForBeacon->fetchAll();
    }
    
    public static function getBooks() {
        self::$getBooks->execute();

        return self::$getBooks->fetchAll();
    }
}
?>