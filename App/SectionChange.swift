import Foundation
import CoreLocation
import UIKit

/**
 *	@author Michaël Minelli, 04.02.2016
 *
 *	Section changement class (respond to the "BeaconDetectionDelegate" delegate)
 */
open class SectionChange: NSObject, BeaconDetectionDelegate {
    fileprivate var bibApp: BibAppAPI
    open var delegate: SectionChangeDelegate
    
    //Beacause some beacons can have the same section, a new beacon is not equal to a new section so we have to verify if a new section is to signal to the delegate
    open var oldSectionID: Int = -1
    
    init(bibApp: BibAppAPI, delegate: SectionChangeDelegate) {
        self.bibApp = bibApp
        self.delegate = delegate
    }
    
    open func beaconDetection(mainBeaconChange beacon:Beacon) {
        //Search the beacon informations (section ID)
        if let beacon = self.bibApp.getBeacon(beacon) {
            //Detect if it's a new section and if yes call the function of the delegate
            if let section = beacon.section {
                if section != self.oldSectionID {
                    self.oldSectionID = section
                    self.delegate.sectionChange(newSection: section)
                }
            }
        }
    }
    
    open func beaconDetection(enterRegion region:CLRegion) {
        /*
        Need a complete coverage of beacon signal - Code ready for notification
        
        let notification = UILocalNotification()
        notification.alertBody = "Bienvenue dans la bibliothèque de hepia. Ouvrez l'application pour profiter de la liste des nouveautés des trois derniers mois ainsi que de la localisation dans la bibliothèque."
        notification.alertAction = "Vers les nouveautés"
        notification.soundName = UILocalNotificationDefaultSoundName
        UIApplication.sharedApplication().scheduleLocalNotification(notification)
        */
    }
}
