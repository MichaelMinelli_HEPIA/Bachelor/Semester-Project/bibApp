#!/usr/bin/python2.7
# -*- coding: utf-8 -*-

from scrapy.spiders import Spider
from scrapy.selector import Selector
from scrapy.http import Request
from scrapy import signals
from scrapy.xlib.pydispatch import dispatcher

from dirbot.items import Book

from hashlib import *

import MySQLdb
import sys
import re
import time
import datetime

HEPIA_ID = "E41"

MySQL_ADDRESS  = "sql01.michaelminelli.ch"
MySQL_DATABASE = "hepia_bibApp"
MySQL_USERNAME = "bibapp_scrapcraw"
MySQL_PASSWORD = "WJs46Nd46sLJ9NTM"

class Book():
    def __init__(self):
        self.id = ""
        self.title = ""
        self.authors = ""
        self.quote = ""
        self.image = ""
        self.resume = ""
        self.language = ""
        self.year = ""
        self.description = ""
        self.editor = ""

    def setYear(self, year):
        match = re.search(r'\d{4}', year)
        self.year = match.group()

    def setLanguage(self, lang):
        if ("Französisch" in lang):
            self.language = "Français"
        elif ("Englisch" in lang):
            self.language = "Anglais"
        elif ("Schweizerdeutsch" in lang or "Deutsch" in lang):
            self.language = "Allemand"
        elif ("Italienisch" in lang):
            self.language = "Italien"
        else:
            self.language = "Non défini"

# http://stackoverflow.com/questions/8798235/dynamic-start-urls-in-scrapy
#
# http://stackoverflow.com/questions/15611605/how-to-pass-a-user-defined-argument-in-scrapy-spider
#
# http://hopefulramble.blogspot.ch/2014/08/web-scraping-with-scrapy-first-steps_30.html
#
# http://recherche.nebis.ch/primo_library/libweb/action/dlDisplay.do?docId=ebi01_prod010540536

class BookSpider(Spider):
    name = "book"
    allowed_domains = ["nebis.ch"]
    start_urls = []
    
    base_url = "http://recherche.nebis.ch/primo_library/libweb/action/"
    
    section = ""
    books = []
    
    def __init__(self, section=None, sectionID=None, *a, **kw):
        super(BookSpider, self).__init__(*a, **kw)

        #scrapy crawl book -a departement=itel
        if not (section and sectionID):
            print "Erreur : Une section et son identifiant doivent être spécifié."
            return

        self.section = section
        self.sectionID = sectionID

        dispatcher.connect(self.spider_closed, signals.spider_closed)

        nbOfMonth = 3
        now = time.localtime()
        lastThreeMonth = [time.localtime(time.mktime((now.tm_year, now.tm_mon - n, 1, 0, 0, 0, 0, 0, 0)))[:2] for n in range(nbOfMonth)]

        #Construct the search string for the last three month
        monthSearch = ""
        firstMonthSearch = True
        for month in lastThreeMonth:
            realMonth = str(month[1])
            if len(realMonth) == 1:
                realMonth = "0" + realMonth

            if firstMonthSearch:
                firstMonthSearch = False
            else:
                monthSearch = monthSearch + "%20OR%20"

            monthSearch = monthSearch + HEPIA_ID + str(month[0]) + realMonth

        self.start_urls.append("http://recherche.nebis.ch/primo_library/libweb/action/search.do?&vl(freeText0)=" + HEPIA_ID + self.section + "%20AND%20%28" + monthSearch + "%29&srt=rank&dscnt=0&mode=Basic&vid=NEBIS&fn=search")
    
    def spider_closed(self):
        try:
            con = MySQLdb.connect(MySQL_ADDRESS, MySQL_USERNAME, MySQL_PASSWORD, MySQL_DATABASE)
        except MySQLdb.Error, e:
            print "Erreur lors de la connexion à la base de données.\n Error %d: %s" % (e.args[0],e.args[1])
            return

        cur = con.cursor()

        for book in self.books:
            try:
                cur.execute("INSERT INTO Book(book_id, book_title, book_authors, book_quote, book_language, book_year, book_description, book_editor, book_section) VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s)", (book.id, book.title, book.authors, book.quote, book.language, book.year, book.description, book.editor, self.sectionID))
            except:
                pass

        con.commit()

        con.close()

        print str(self.booksCounter) + " ont été trouvé."

    #List of page already send to parsing
    pagesNumber = set((1,))
    def parse(self, response):
        sel = Selector(response)
        
        # Find all pages to explore
        pages = []
        try:
            pages = sel.xpath('//a[@class="EXLPrevious EXLBriefResultsPaginationLinkPrevious EXLBriefResultsPagination"]')
        except:
            pass

        for page in pages:
            try:
                #If its a page number (don't take arrows)
                pageNumber = int(page.xpath('text()').extract()[0])
                
                #Test if this number of page is already to explore (numbers of pages are two times in the page and we need to verify numbers for every page because only 5 number of pages are show so there is a pseudo scroll)
                if not pageNumber in self.pagesNumber:
                    self.pagesNumber.add(pageNumber)
                    yield Request(str(page.xpath('@href').extract()[0]), callback = self.parse) #Explore the page
            except:
                pass

        books = sel.xpath('//div[@class="EXLSummaryFields"]')

        #For every books found
        for book in books:
            path = book.xpath('h2[@class="EXLResultTitle"]/a/@href').extract()[0]

            #Test if its a book with multiple versions and if yes, call the parser for that
            if book.xpath('span[@class="EXLResultVersionFound"]').extract():
                yield Request(path, callback = self.parseMultipleVersion)
            else:
                #Else call the parser of a book
                link = self.base_url + path
                yield Request(link, callback = self.parseBook)
        
        return

    #Parse the page of multiple versions of the book
    def parseMultipleVersion(self, response):
        sel = Selector(response)
        books = sel.xpath('//div[@class="EXLSummaryFields"]')
        for book in books:
            path = self.base_url + book.xpath('h2[@class="EXLResultTitle"]/a/@href').extract()[0]
            #Return (not yield) the first book of the list (most recent)
            return Request(path, callback = self.parseBook)

    booksCounter = 0
    #Parse a page of a book and insert it to the database if necessary
    def parseBook(self, response):
        sel = Selector(response)

        currentBook = Book()

        #Find the id of the book
        for arg in response.url.split('&'):
            dic = arg.split('=')
            if dic[0] == "doc":
                currentBook.id = dic[1]
                break;

        try:
            currentBook.title = sel.xpath('//div[@class="EXLLinkedFieldTitle"]/text()').extract()[0].encode('utf-8')
        except:
            pass
        
        try:
            currentBook.authors = sel.xpath('//h2[@class="EXLResultAuthor"]/text()').extract()[0].encode('utf-8')
        except:
            pass

        try:
            currentBook.setLanguage(sel.xpath('//li[@id="Sprache-1"]').extract()[0].encode('utf-8'))
        except:
            pass

        try:
            currentBook.setYear(sel.xpath('//li[@id="Erscheinungsdatum-1"]/span[@class="EXLDetailsDisplayVal"]/text()').extract()[0].encode('utf-8'))
        except:
            pass

        try:
            currentBook.description = sel.xpath('//li[@id="Beschreibung-1"]/span[@class="EXLDetailsDisplayVal"]/text()').extract()[0].encode('utf-8')
        except:
            pass

        try:
            currentBook.editor = sel.xpath('//li[@id="Ort, Verlag-1"]/span[@class="EXLDetailsDisplayVal"]/text()').extract()[0].encode('utf-8')
        except:
            pass
        
        #currentBook.quote = ""
        #currentBook.image = ""
        #currentBook.resume = ""
        #currentBook.applicant = ""

        self.books.append(currentBook)

        self.booksCounter += 1

        return None