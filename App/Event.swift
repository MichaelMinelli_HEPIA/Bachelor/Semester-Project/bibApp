import Foundation

/**
 *  @author Michaël Minelli, 16.01.2016
 *
 *  This is one of the simplest mechanisms for communication between threads: one thread signals an event and other threads wait for it.
 *  An event object manages an internal flag that can be set to true with the set() method and reset to false with the clear() method. The wait() method blocks until the flag is true.
 *
 *  This class is a replication of the Event object of python : https://docs.python.org/2/library/threading.html#threading.Event
 */
open class Event {
    fileprivate var waitingSemaphore: DispatchSemaphore = DispatchSemaphore(value: 0)
    fileprivate var counterWait:Int = 0 //Counter of waiting thread on the value of the flag
    
    fileprivate var mutexFlag: DispatchSemaphore = DispatchSemaphore(value: 1) //Mutex for protect the access to the flag (and the counter by the same time)
    fileprivate var flag:Bool
    
    /**
     Constructor
     
     - parameter flagValue:	Initial value of the flag
     */
    init(flagValue:Bool = true) {
        flag = flagValue
    }
    
    /**
     Return the flag value without block
     
     - returns: The flag value
     */
    open func isSet()->Bool {
        mutexFlag.wait(timeout: DispatchTime.distantFuture)
        let result = flag
        mutexFlag.signal()
        
        return result
    }
    
    /**
     Set the value of the flag to true and free all thread which wait to it
     */
    open func set() {
        mutexFlag.wait(timeout: DispatchTime.distantFuture)
        flag = true
        
        //Free all thread
        if counterWait > 0 {
            for _ in 1...counterWait {
                waitingSemaphore.signal()
            }
        }
        
        counterWait = 0
        mutexFlag.signal()
    }
    
    /**
     Set the value of the flag to false
     */
    open func clear() {
        mutexFlag.wait(timeout: DispatchTime.distantFuture)
        flag = false
        mutexFlag.signal()
    }
    
    /**
     If the flag is false, block until the flag become true
     */
    open func wait() {
        mutexFlag.wait(timeout: DispatchTime.distantFuture)
        
        if !flag {
            //Increment the counter of waiting threads
            counterWait += 1
            mutexFlag.signal()
            waitingSemaphore.wait(timeout: DispatchTime.distantFuture)
        } else {
            mutexFlag.signal()
        }
    }
}
