from scrapy.spider import Spider
from scrapy.selector import Selector

from dirbot.items import Website


class DmozSpider(Spider):
    name = "dmoz"
    allowed_domains = ["nebis.ch"]




    start_urls = [
    	""
        "http://recherche.nebis.ch/primo_library/libweb/action/search.do?&vl(freeText0)=E41itel%20AND%20%28E41201509%20OR%20E41201510%29%20&srt=rank&dscnt=0&mode=Basic&vid=NEBIS&fn=search"
        #"http://recherche.nebis.ch/primo_library/libweb/action/search.do?&vl(freeText0)=E41cult%20AND%20%28E41201509%20OR%20E41201510%29%20&srt=rank&dscnt=0&mode=Basic&vid=NEBIS&fn=search"
    ]

    def parse(self, response):
        sel = Selector(response)
				
        sites = sel.xpath('//div[@class="EXLSummaryFields"]')
        items = []

        for site in sites:
        	print(site.xpath('h2[@class="EXLResultTitle"]/a/text()').extract())
            #item = Website()
            #item['name'] = site.xpath('a/text()').extract()
            #item['url'] = site.xpath('a/@href').extract()
            #item['description'] = site.xpath('text()').re('-\s[^\n]*\\r')
            #items.append(item)

        return items
