import Foundation
import UIKit

/**
 *	@author Michaël Minelli, 16.01.2016
 *
 *	List of colors used in the project
 */
public struct Colors {
    public static let hepia = UIColor(red: 189/255, green: 12/255, blue: 17/255, alpha: 1.0)
    
    public static let lineBackground1 = UIColor.white
    public static let lineBackground2 = UIColor(white: 0.96, alpha: 1)
    
    public static let tabBarItemNormal = UIColor.white
    public static let tabBarItemSelected = UIColor.white
    
    public static let primaryColor = Colors.hepia
    public static let secondaryColor = UIColor.white
    
    
    public static let darkGreen = UIColor(red: 0/255, green: 153/255, blue: 51/255, alpha: 1.0)
}
