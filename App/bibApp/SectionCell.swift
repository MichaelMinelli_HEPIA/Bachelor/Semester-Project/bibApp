import UIKit

class SectionCell: UITableViewCell {
    
    @IBOutlet weak var imgSection: UIImageView!
    @IBOutlet weak var lblSection: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func prepareCell(_ section: Section) {
        lblSection.text = section.name
        imgSection.image = section.image
    }
}
