import UIKit

class BookMinCell: UITableViewCell, BookCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblAuthors: UILabel!
    @IBOutlet weak var lblYear: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func prepareCell(_ book: Book) {
        lblTitle.text = book.title
        lblAuthors.text = book.author
        lblYear.text = book.year
    }
}
