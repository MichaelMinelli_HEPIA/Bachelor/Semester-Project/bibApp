import UIKit

class SettingsController: UIViewController, UIActionSheetDelegate {
    
    @IBOutlet weak var toolBar: UIToolbar!
    @IBOutlet weak var swBookListImgStyle: UISwitch!
    @IBOutlet weak var swBeaconDetection: UISwitch!
    @IBOutlet weak var lblBeaconDetectionTime: UILabel!
    
    var beaconDetectionPreference = BeaconDetectionPreference();
    
    var beaconDetectionDuration: [Int] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Global_IOS.setGlobalAppearance(self, withSettingsButton: false)
        
        //Prepare the tool bar
        let space = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let item = UIBarButtonItem(title: "Retour à l'application", style: .done, target: self, action: #selector(SettingsController.closeButtonClick(_:)))
        let space2 = UIBarButtonItem(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
        
        toolBar.setItems([space, item, space2], animated: true)
        
        beaconDetectionPreference = Global_IOS.preferences.getBeaconDetectionPref()
        
        //Set the value to the current user preferences
        swBookListImgStyle.setOn(!Global_IOS.preferences.isBookListMinStyle(), animated: false)
        swBeaconDetection.setOn(self.beaconDetectionPreference.isEnabled(Global_IOS.preferences), animated: false)
        
        //Test the value of the beacon detection preference and display the right string in the label
        displayDisabledTime()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    /**
     Test the value of the beacon detection preference and display the right string in the label
     */
    func displayDisabledTime() {
        if !self.beaconDetectionPreference.isEnabled(Global_IOS.preferences) {
            //Calculation of the since since the end of the desactivation
            let time = Double(self.beaconDetectionPreference.duration) - self.beaconDetectionPreference.timeIntervaelSinceDesactivation()
            lblBeaconDetectionTime.text = "Réactivation auto. dans \(Int(time / 86400)) jours \(Int((time.truncatingRemainder(dividingBy: 86400)) / 3600)) heures et \(Int(((time.truncatingRemainder(dividingBy:86400)).truncatingRemainder(dividingBy: 3600)) / 60)) minutes"
        } else {
            lblBeaconDetectionTime.text = "Activée";
        }
    }
    
    func closeButtonClick(_ sender: AnyObject) {
        //Close settings
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func swBookListImgStyleValueChanged(_ sender: UISwitch) {
        Global_IOS.preferences.setBookListMinStyle(!sender.isOn)
    }
    
    @IBAction func swBeaconDetectionValueChanged(_ sender: UISwitch) {
        if sender.isOn {
            self.beaconDetectionPreference.enable()
            Global_IOS.preferences.setBeaconDetectionPref(self.beaconDetectionPreference)
            displayDisabledTime()
        } else {
            //Ask for the time the user want to disable the beacon detection
            beaconDetectionDuration = [3600, 14400, 86400, 604800, 2678400]
            
            let actionSheet = UIActionSheet(title: "Pour combien de temps voulez-vous désactiver la localisation ?", delegate: self, cancelButtonTitle: "Annuler", destructiveButtonTitle: nil, otherButtonTitles: "1 heure", "4 heures", "1 jour", "1 semaine", "1 mois")
            actionSheet.show(in: self.view)
        }
    }
    
    func actionSheet(_ actionSheet: UIActionSheet, clickedButtonAt buttonIndex: Int) {
        //Test if the user change his minds
        if buttonIndex == actionSheet.cancelButtonIndex {
            swBeaconDetection.setOn(true, animated: true)
        } else {
            beaconDetectionPreference.disable(self.beaconDetectionDuration[buttonIndex-1])
            Global_IOS.preferences.setBeaconDetectionPref(beaconDetectionPreference)
        }
        
        displayDisabledTime()
    }
}

