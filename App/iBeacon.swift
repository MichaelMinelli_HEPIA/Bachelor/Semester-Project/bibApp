import Foundation

/**
 *	@author Michaël Minelli, 16.01.2016
 *
 *	Datas of an iBeacon beacon
 */
open class iBeacon: NSObject, Beacon, NSCoding {
    open var section: Int?
    
    open var major: Int
    open var minor: Int
    
    init(section: Int?, major: Int, minor: Int) {
        self.section = section
        self.major = major
        self.minor = minor
    }
    
    public required init(coder aDecoder: NSCoder) {
        self.section = aDecoder.decodeInteger(forKey: "section")
        self.major = aDecoder.decodeInteger(forKey: "major")
        self.minor = aDecoder.decodeInteger(forKey: "minor")
    }
    
    open func encode(with aCoder: NSCoder) {
        aCoder.encode(self.section!, forKey: "section")
        aCoder.encode(self.major, forKey: "major")
        aCoder.encode(self.minor, forKey: "minor")
    }
}
