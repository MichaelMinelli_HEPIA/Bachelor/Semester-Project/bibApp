import Foundation

/**
 *	@author Michaël Minelli, 16.01.2016
 *
 *	Response of a HTTP Request
 */
public struct HTTPRequestResult {
    public var responseCode: Int = HTTPStatusCode.OK
    
    public var response: String = ""
    
    //Response which is formating by json function (AnyObject for let determine which object it's really)
    public var jsonObject: Any = -1 as Any
    
    mutating func affectResponse(_ data: Data) {
        self.response = String(data: data, encoding: String.Encoding.utf8)!
        
        //Transform response to corresponding object (not detemine in advance so AnyObject)
        do {
            self.jsonObject = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers)
        } catch {}
    }
}
