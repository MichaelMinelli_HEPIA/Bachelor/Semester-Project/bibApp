import UIKit

class BookController: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate {
    
    var allSections: Bool = false
    var section:Section = Section(id: -1, name: "", acronym: "", code: "")
    
    //List contains all books of this section (or all books)
    var books: [Book] = []
    //List contains books which are satisfy the current search string. It's this list which is display to the user
    var booksToShow: [Book] = []
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Global_IOS.setGlobalAppearance(self, withSettingsButton: false)
        
        //Get all books of the section or of all sections
        self.books = (self.tabBarController! as! SectionTabBarController).allSections ? Global_IOS.bibApp.getBooks() : Global_IOS.bibApp.getBooks(forSection: (self.tabBarController! as! SectionTabBarController).section)
        self.booksToShow = self.books
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.tableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.booksToShow.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //Set the size in function of the style of the list wanted by the user
        return Global_IOS.preferences.isBookListMinStyle() ? 82 : 200
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell
        
        //Get the cell in function of the style of the list wanted by the user
        if Global_IOS.preferences.isBookListMinStyle() {
            cell = tableView.dequeueReusableCell(withIdentifier: "BookMinCell", for: indexPath)
        } else {
            cell = tableView.dequeueReusableCell(withIdentifier: "BookImgCell", for: indexPath)
        }
        
        //Prepare the cell with book information
        (cell as! BookCell).prepareCell(self.booksToShow[indexPath.row])
        
        //Alternate the background color
        cell.backgroundColor = indexPath.row % 2 == 1 ? Colors.lineBackground2 : Colors.lineBackground1
        
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //Send the book information to the next view
        if let row = self.tableView.indexPathForSelectedRow?.row {
            (segue.destination as! BookDetailsController).book = self.booksToShow[row]
        }
    }
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        return true
    }
    
    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool // return NO to not resign first responder
    {
        return true
    }
    
    /**
     Do the search in the main list for compose the display list
     
     - parameter searchText: search string
     */
    func searchAction(_ searchText: String) {
        var searchText = searchText
        searchText = searchText.lowercased()
        
        //Reduction of the main list in function of the search string.
        //The search string is compare to the title, the authors and the year of books
        self.booksToShow = searchText == "" ? self.books : self.books.filter({$0.title.lowercased().contains(searchText) || $0.author.lowercased().contains(searchText) || $0.year.lowercased().contains(searchText)})
        
        //Reload visualisation of books
        tableView.reloadData()
    }
    
    func textChange(_ searchBar: UISearchBar!, searchText: String!) {
        searchAction(searchText)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        textChange(searchBar, searchText: searchText)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        //hide the keyboard
        self.view.endEditing(true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        //hide the keyboard
        self.view.endEditing(true)
    }
}

