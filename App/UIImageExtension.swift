import Foundation
import UIKit

extension UIImage {
    /**
     Change the main color of an image. Principally for mono color icon
     
     - source: http://stackoverflow.com/questions/25052729/default-tab-bar-item-colors-using-swift-xcode-6
     
     - parameter tintColor: New color of the icon
     
     - returns: New UIImage with the new color
     */
    func imageWithColor(_ tintColor: UIColor) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
        
        let context = UIGraphicsGetCurrentContext()! as CGContext
        context.translateBy(x: 0, y: self.size.height)
        context.scaleBy(x: 1.0, y: -1.0);
        context.setBlendMode(CGBlendMode.normal)
        
        let rect = CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height) as CGRect
        context.clip(to: rect, mask: self.cgImage!)
        tintColor.setFill()
        context.fill(rect)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext() as! UIImage
        UIGraphicsEndImageContext()
        
        return newImage
    }
}
