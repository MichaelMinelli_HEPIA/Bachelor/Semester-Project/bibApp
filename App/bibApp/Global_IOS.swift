import Foundation
import CoreLocation
import CoreBluetooth
import UIKit

/**
 *	@author Michaël Minelli, 16.01.2016
 *
 *	Global functions and variables used in all the iOS programm
 */
open class Global_IOS {
    //BibApp object
    open static var bibApp: BibAppAPI = BibAppAPI(loadAtCreation: false, fromLocalStorage: true)
    
    //Object for access to preference stockage
    open static var preferences: Preferences = Preferences()
    
    open static var ibeaconDetection: iBeaconDetection?
    
    //Beacon Region Object
    open static var beaconRegion: CLBeaconRegion = CLBeaconRegion(proximityUUID: UUID(uuidString: "F4CC7898-13C1-0D0B-972C-02D7E98F3F87")!, identifier: "ch.minelli.bibApp.bookRegion")
    fileprivate static var isAlreadyBluetoothNotified = false
    fileprivate static var isAlreadyConnexionNotified = false
    
    /**
     Create the global appearance of the app (color of the navigation, etc.)
     
     - parameter navigationController:	The navigation controller object
     - parameter settings:              Boolean wich tell if a settings button have to be add in navigation bar
     - parameter tabBarController:                Optional TabBar Controller for initiate the style
     */
    open static func setGlobalAppearance(_ viewController: UIViewController, withSettingsButton settings: Bool, withTabBarController tabBarController: UITabBarController? = nil) {
        //Color
        viewController.preferredStatusBarStyle
        viewController.navigationController!.navigationBar.backgroundColor = Colors.primaryColor
        viewController.navigationController!.navigationBar.barTintColor = Colors.primaryColor
        viewController.navigationController!.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : Colors.secondaryColor]
        viewController.navigationController!.navigationBar.tintColor = Colors.secondaryColor
        viewController.navigationController!.navigationBar.isTranslucent = false
        
        //Settings Button
        if settings {
            viewController.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "SettingsIcon"), style: .plain, target: viewController, action: "settingsButtonClick:")
        }
        
        //Tab bar style
        if let tabBarController = tabBarController {
            tabBarController.tabBar.backgroundColor = Colors.primaryColor
            UITabBarItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: Colors.tabBarItemNormal], for:UIControlState())
            UITabBarItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: Colors.tabBarItemSelected], for:.selected)
            
            //Tab bar icon colors
            for item in tabBarController.tabBar.items! {
                if let image = item.image {
                    item.image = image.imageWithColor(Colors.tabBarItemNormal).withRenderingMode(.alwaysOriginal)
                }
                
                if let selectedImage = item.selectedImage {
                    item.selectedImage = selectedImage.imageWithColor(Colors.tabBarItemSelected).withRenderingMode(.alwaysOriginal)
                }
            }
        }
    }
    
    /**
     Open the settings view
     
     - parameter viewController: Current view controller
     */
    open static func openSettingsView(_ viewController: UIViewController) {
        let vc : UIViewController = viewController.storyboard!.instantiateViewController(withIdentifier: "navigationSettings") as UIViewController
        viewController.present(vc, animated: true, completion: nil)
    }
    
    /**
     Start the beacon detection
     
     - parameter viewController: Current view controller
     */
    open static func runBeaconDetection(_ viewController: UIViewController) {
        
        //If the beacon detection is enabled
        if Global_IOS.preferences.getBeaconDetectionPref().isEnabled(Global_IOS.preferences) {
            //Test if the variable is init
            if let ibeaconDetection = Global_IOS.ibeaconDetection {
                //Update the current viewController
                ((ibeaconDetection.delegate as! SectionChange).delegate as! SectionChangeAction).viewController = viewController
            } else {
                //Otherwise init it with the delegate (instance of the SectionChange class)
                Global_IOS.ibeaconDetection = iBeaconDetection(region: Global_IOS.beaconRegion, delegate: SectionChange(bibApp: Global_IOS.bibApp, delegate: SectionChangeAction(viewController: viewController)))
            }
            
            func showMessage(_ title: String, message: String) {
                SCLAlertView().showCustom(title, subTitle: message, duration: nil, completeText: "Ok", iconImage: UIImage(named: "bibApp"), viewColor: Colors.primaryColor, colorTextButton: Colors.secondaryColor)
            }
            
            //test if bluetooth connexion is available
            let bluetoothManagerState = CBCentralManager().state
            
            if bluetoothManagerState == CBManagerState.poweredOn || bluetoothManagerState == CBManagerState.unknown {
                do {
                    //Run the detection
                    try self.ibeaconDetection?.run()
                } catch (iBeaconDetectionError.locationAccessDenied) {
                    if !isAlreadyConnexionNotified {
                        isAlreadyConnexionNotified = true
                        
                        showMessage("Autorisations", message: "La fonctionnalité de localisation à l'intérieur de la bibliothèque nécessite les droits de localisation. Vous pouvez les accorder à partir de l'application 'Réglages'.")
                    }
                } catch {}
            } else if bluetoothManagerState == CBManagerState.poweredOff || bluetoothManagerState == CBManagerState.unauthorized {
                if !isAlreadyBluetoothNotified {
                    isAlreadyBluetoothNotified = true
                    
                    showMessage("Bluetooth", message: "La fonctionnalité de localisation à l'intérieur de la bibliothèque nécessite que le bluetooth soit actif. Vous pouvez l'activer à partir de l'application 'Réglages'.")
                }
            }
        }
    }
}
