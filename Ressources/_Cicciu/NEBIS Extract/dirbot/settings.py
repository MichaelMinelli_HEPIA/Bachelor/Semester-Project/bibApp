# Scrapy settings for dirbot project

LOG_ENABLED=False
LOG_LEVEL = 'INFO'

SPIDER_MODULES = ['dirbot.spiders']
NEWSPIDER_MODULE = 'dirbot.spiders'
CONCURRENT_REQUESTS = 1
#DEFAULT_ITEM_CLASS = 'dirbot.items.Website'

#ITEM_PIPELINES = {'dirbot.pipelines.FilterWordsPipeline': 1}
