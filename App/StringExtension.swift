import Foundation

extension String {
    /**
     Encode a string in a another string with the url format (example : a space become %20)
     
     - returns: The string with URL format
     */
    func urlEncode() -> String {
        //Encode the majority of characters
        let urlEncoded: String = self.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        var result: String = ""
        
        //Encode the rest that the previous function don't do
        for i in urlEncoded.characters {
            if i == "&" {
                result.append("%26")
            } else if i == "/" {
                result.append("%2F")
            } else {
                result.append(i)
            }
        }
        
        return result
    }
}
