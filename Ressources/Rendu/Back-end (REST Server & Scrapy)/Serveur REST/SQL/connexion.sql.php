<?php

class SQLConnect
{
    public static $db;

    public static function setDatabase($db)
    {
        self::$db = $db;
    }
}

try {
	$connect = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_DBNAME . ';port=' . DB_PORT, DB_USERNAME, DB_PASSWORD);
	$connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$connect->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
	$connect->exec("SET CHARACTER SET utf8");
	SQLConnect::setDatabase($connect);
} catch (Exception $e) {
	http_response_code(500);
	die("ERREUR : Connexion à la base de données impossible.");
}



?>