//
//  ViewController.swift
//  DecouverteEstimoteSDK
//
//  Created by Michaël Minelli on 11.10.15.
//  Copyright © 2015 Michaël Minelli. All rights reserved.
//

import UIKit

class ViewController: UIViewController, ESTBeaconManagerDelegate {
    
    @IBOutlet weak var txtShow: UITextView!
    
    var beaconManager: ESTBeaconManager!
    
    //CLBeaconRegion(proximityUUID: NSUUID(UUIDString: "B9407F30-F5F8-466E-AFF9-25556B57FE6D")!, major: CLBeaconMajorValue(402), minor: CLBeaconMinorValue(1), identifier: "ch.minelli.ch.minelli.DecouverteEstimoteSDK.ViewController") //Beacon bleu clair
    var beaconRegion: CLBeaconRegion! = CLBeaconRegion(proximityUUID: NSUUID(UUIDString: "B9407F30-F5F8-466E-AFF9-25556B57FE6D")!, identifier: "ch.minelli.DecouverteEstimoteSDK.ViewController")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Création du Location Manager ainsi qu'indication à celui-ci que sont delegate est l'objet courant
        beaconManager = ESTBeaconManager()
        beaconManager.delegate = self
        
        //Permet d'indiquer au système de toujours chercher les beacons même lorsque l'application est fermée
        beaconRegion.notifyOnEntry = true
        beaconRegion.notifyOnExit = true
        
        //Vérifie et demande les droits nécessaire à la localisation
        if CLLocationManager.locationServicesEnabled() {
            if ESTBeaconManager.authorizationStatus() ==  CLAuthorizationStatus.NotDetermined {
                beaconManager.requestAlwaysAuthorization() //Demande l'autorisation de toujours pouvoir chercher des balises
            } else if ESTBeaconManager.authorizationStatus() == CLAuthorizationStatus.Denied {
                let message = UIAlertView(title: "Acces refusée", message: "Vous avez refusé l'accès à la localisation", delegate: nil, cancelButtonTitle: "Ok")
                message.show()
            } else if ESTBeaconManager.authorizationStatus() == CLAuthorizationStatus.Restricted {
                let message = UIAlertView(title: "Localisation non disponible", message: "Nous n'avons pas accès à la localisation", delegate: nil, cancelButtonTitle: "Ok")
                message.show()
            }
            
            //Lance la « localisation »
            beaconManager.startMonitoringForRegion(self.beaconRegion)
        } else {
            let message = UIAlertView(title: "Erreur de localisation", message: "Localisation non activée", delegate: nil, cancelButtonTitle: "Ok")
            message.show()
        }
    }
    
    func beaconManager(manager: AnyObject, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        if status != CLAuthorizationStatus.NotDetermined && status != CLAuthorizationStatus.Denied {
            beaconManager.startMonitoringForRegion(self.beaconRegion)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidDisappear(animated: Bool) {
        beaconManager.stopRangingBeaconsInRegion(self.beaconRegion)
    }
    
    // Est appelée quand un beacon commence à être détecté (entre dans la région de réception)
    func beaconManager(manager: AnyObject, didEnterRegion region: CLBeaconRegion) {
        let notification = UILocalNotification()
        notification.alertBody = "Entree dans la région" // text that will be displayed in the notification
        notification.alertAction = "région" // text that is displayed after "slide to..." on the lock screen - defaults to "slide to view"
        notification.soundName = UILocalNotificationDefaultSoundName
        notification.category = "REGION_CATEGORY"
        UIApplication.sharedApplication().presentLocalNotificationNow(notification)
    }
    
    // Est appelée quand plus aucun beacon n'est détecté (sort de la région de réception)
    func beaconManager(manager: AnyObject, didExitRegion region: CLBeaconRegion) {
        let notification = UILocalNotification()
        notification.alertBody = "Sortie de la région" // text that will be displayed in the notification
        notification.alertAction = "région" // text that is displayed after "slide to..." on the lock screen - defaults to "slide to view"
        notification.soundName = UILocalNotificationDefaultSoundName // play default sound
        //notification.userInfo = ["UUID": item.UUID, ] // assign a unique identifier to the notification so that we can retrieve it later
        notification.category = "REGION_CATEGORY"
        UIApplication.sharedApplication().presentLocalNotificationNow(notification)
    }
    
    func beaconManager(manager: AnyObject, didStartMonitoringForRegion region: CLBeaconRegion) {
        //Demande également les mises à jour sur l'état de la région
        beaconManager.requestStateForRegion(region)
    }
    
    func beaconManager(manager: AnyObject, didDetermineState state: CLRegionState, forRegion region: CLBeaconRegion) {
        //Si nous somme à proximité de balises, commence la détecton des balises en continu sinon la désactive
        
        if state == CLRegionState.Inside {
            beaconManager.startRangingBeaconsInRegion(beaconRegion)
        }
        else {
            beaconManager.stopRangingBeaconsInRegion(beaconRegion)
        }
    }
    
    // Voilà la partie la plus intéréssante, la découverte des balises
    func beaconManager(manager: AnyObject, didRangeBeacons beacons: [CLBeacon], inRegion region: CLBeaconRegion) {
        //Extrait tout les beacons dont on connais la distance
        let knownBeacons = beacons.filter{ $0.proximity != CLProximity.Unknown }
        for beacon in knownBeacons {
            var proximityMessage: String
            
            switch beacon.proximity {
            case CLProximity.Immediate:
                proximityMessage = "Très proche"
                
            case CLProximity.Near:
                proximityMessage = "Proche"
                
            case CLProximity.Far:
                proximityMessage = "Loin"
                
            default:
                proximityMessage = "Pas possible avec le filtre plus haut"
            }
            
            txtShow.text = "\(txtShow.text)\n\(beacon.major).\(beacon.minor) \(proximityMessage) +/- \(beacon.accuracy)m \(beacon.rssi) "
        }
    }
    
    //Gestion des erreurs
    func beaconManager(manager: AnyObject, didFailWithError error: NSError) {
        let message = UIAlertView(title: "Erreur de monitoring", message: error.localizedDescription, delegate: nil, cancelButtonTitle: "Ok")
        message.show()
    }
    
    func beaconManager(manager: AnyObject, monitoringDidFailForRegion region: CLBeaconRegion?, withError error: NSError) {
        let message = UIAlertView(title: "Erreur de monitoring", message: error.localizedDescription, delegate: nil, cancelButtonTitle: "Ok")
        message.show()
    }
    
    func beaconManager(manager: AnyObject,rangingBeaconsDidFailForRegion region: CLBeaconRegion?,withError error: NSError){
        let message = UIAlertView(title: "Erreur de ranging", message: error.localizedDescription, delegate: nil, cancelButtonTitle: "Ok")
        message.show()
    }
}

