import Foundation

/**
 *	@author Michaël Minelli, 16.01.2016
 *
 *	List of useful HTTP status code
 */
public struct HTTPStatusCode {
    public static let ERROR: Int = -1
    public static let OK: Int = 200
}