<?php

class NearablesSQL extends SQLConnect
{
    private static $getAll, $getBooksForBeacon, $getBooks;

    public static function prepare() {
        self::$getAll = self::$db->prepare(
            "SELECT nearables_id as 'id', nearables_section as 'section' FROM Nearables WHERE nearables_status = 1;"
        );
        
        self::$getBooksForBeacon = self::$db->prepare(
	        "SELECT book_id as 'id', book_title as 'title', book_authors as 'author', book_quote as 'quote', book_image as 'image', book_resume as 'resume', book_language as 'language', book_applicant as 'applicant', book_year as 'year', book_description as 'description', book_editor as 'editor', book_section as 'section' FROM Book JOIN Nearables ON book_section = nearables_section WHERE nearables_id=:id ORDER BY book_insert_date DESC;"
        );
        
        self::$getBooks = self::$db->prepare(
            "SELECT book_id as 'id', book_title as 'title', book_authors as 'author', book_quote as 'quote', book_image as 'image', book_resume as 'resume', book_language as 'language', book_applicant as 'applicant', book_year as 'year', book_description as 'description', book_editor as 'editor', book_section as 'section' FROM Book WHERE book_section IN (SELECT DISTINCT(nearables_section) FROM Nearables WHERE nearables_status = 1) ORDER BY book_insert_date DESC;"
        );
    }

    public static function getAll() {
        self::$getAll->execute();

        return self::$getAll->fetchAll();
    }
    
    public static function getBooksForBeacon($id) {
        self::$getBooksForBeacon->execute(array(
	        ':id' => $id
        ));

        return self::$getBooksForBeacon->fetchAll();
    }
    
    public static function getBooks() {
        self::$getBooks->execute();

        return self::$getBooks->fetchAll();
    }
}
?>